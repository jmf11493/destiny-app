import { Component, OnInit } from '@angular/core';
import { NgForm, FormGroup } from '@angular/forms';
import { RestService } from '../rest.service';
import { ActivatedRoute, Router } from '@angular/router';
import * as $ from 'jquery';
import 'datatables.net';

@Component({
  selector: 'app-weapon',
  templateUrl: './weapon.component.html',
  styleUrls: ['./weapon.component.css']
})
export class WeaponComponent implements OnInit {
    public weaponTable:any;

    weapons:any = [];
    hasWeapons:any = false;

    constructor( public rest:RestService, 
                private route:ActivatedRoute,
                private router:Router ) { }

    ngOnInit():void {
    }
    
    private reInitTable():void {
        if ( this.weaponTable ) {
            this.weaponTable.destroy();
            this.weaponTable = null;
        }
        setTimeout( () => this.initTable(), 0 );
    }
        
    private initTable():void {
        let tableId:any = $( '#weaponResults' );
        this.weaponTable = tableId.DataTable( {
            "select": true,
            "scrollY": "400px",
            "scrollCollapse": true
        });
    }

    getWeapons( user:NgForm ) {
        this.rest.getWeapons( user.value.username, user.value.systemSelected ).subscribe(( data: {} ) => {
            this.weapons = data;
            this.hasWeapons = this.weapons.length > 0;
            this.reInitTable();
        });
    }
}
