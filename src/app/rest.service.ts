import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { Observable, of } from 'rxjs';
import { map, catchError, tap } from 'rxjs/operators';


const endpoint = 'http://localhost:8080/api/v1/';
// TODO headers go here
const httpOptions = {
  headers: new HttpHeaders({
    //'Content-Type':  'application/json'
  })
};

@Injectable({
  providedIn: 'root'
})
export class RestService {

  constructor( private http:HttpClient ) { }
  
  private returnData( response:Response ) {
    return response;
  }
  
  getWeapons( username, system ): Observable<any> {
    return this.http.get( endpoint + 'weapons?username=' + username + '&system=' + system )
        .pipe( map( this.returnData ) );
  }
}
