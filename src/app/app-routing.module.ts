import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { WeaponComponent } from './weapon/weapon.component';

const routes: Routes = [
    {
        path: 'weapons',
        component: WeaponComponent,
        data: { title: 'Test Weapon Component' }
    }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
