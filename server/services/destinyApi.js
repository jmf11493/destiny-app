var config = require( "../config/config" );
var request = require( "request" );

class DestinyApi {
    
    constructor() {
        this.apiKey  = config.configFile.key;
        this.baseUrl = "https://www.bungie.net/d1/platform/destiny/";
        this.membershipEndpoint = "/Stats/GetMembershipIdByDisplayName/";
        this.accountEndpoint = "/Account/";
        this.weaponEndpoint = "Stats/UniqueWeapons/";
        this.weaponInfoEndpoint = "Manifest/InventoryItem/";
    }

    getMembershipId( system, username, callback ) {
        var url = this.baseUrl + system + this.membershipEndpoint + username;
        var options = this.getOptions( url );

        request( options, callback );
    }

    getCharacterIds( system, membershipId, callback ) {
        var url = this.baseUrl + system + this.accountEndpoint + membershipId;
        var options = this.getOptions( url );
        
        request( options, callback );
    }
    
    getUniqueWeapons( membershipId, characterId, system, callback ) {
        var url = this.baseUrl + this.weaponEndpoint + system + "/" + membershipId + "/" + characterId;
        var options = this.getOptions( url );
        
        request( options, callback );
    }
    
    getExoticWeapons( system, username, callback ) {
        var me = this;
        
        // TODO fix callback hell
        function membershipCallback( error, res, body ) {
            if( !me.checkError( "Membership id call", error, res, body ) ) {
                var membershipId = JSON.parse( body ).Response;
                me.getCharacterIds( system, membershipId, function( error, res, body ) {
                    if( !me.checkError( "Character id call", error, res, body ) ) {
                        var characterId = JSON.parse( body ).Response.data.characters[ 0 ].characterBase.characterId;
                        me.getUniqueWeapons( membershipId, characterId, system, callback );
                    }
                });
            }
        }

        this.getMembershipId( system, username, membershipCallback );
    }
    
    getWeaponInformation( referenceId, callback ) {
        var url = this.baseUrl + this.weaponInfoEndpoint + referenceId;
        var options = this.getOptions( url );
        
        request( options, callback );
    }
    
    getOptions( url ) {
        return {
            url: url,
            headers: {
                "X-API-Key": this.apiKey
            }
        };
    }
    
    checkError( methodCall, error, response, body ) {
        var hasError = false;
        var jsonBody = this.isJson( body ) ? JSON.parse( body ) : false;
        
        if( error ) {
            hasError = true;
            console.error( "Error: ", error );
        }
        
        if( jsonBody && jsonBody.ErrorCode !== 1 ) {
            hasError = true;
            console.error( "Error Code: ", jsonBody.ErrorCode );
            console.error( "Error Status: ", jsonBody.ErrorStatus );
            console.error( "Error Message: ", jsonBody.Message );
        }
        
        if( !jsonBody ) {
            hasError = true;
            // We didn't get a JSON response, assume something went wrong
            console.error( "Error in response body: ", body );
        }
        
        if ( hasError ) {
            console.error( "Error in method call: " + methodCall );
        }
        
        return hasError;
    }
    
    isJson( string ) {
        try {
            JSON.parse( string );
        } catch( e ) {
            return false;
        }
        return true;
    }
}

module.exports = DestinyApi;

