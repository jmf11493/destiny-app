const express = require( 'express' );
const router = express.Router();

const DestinyApi = require( "../services/destinyApi.js" );
// Routes for REST API

router.get( '/v1/weapons', ( request, response ) => {
    // Get the users online id from the request
    var username = request.query.username;
    // Get the users console selection
    var system = parseInt( request.query.system );
    // Do server side validation
    if ( validateData( username, system ) ) {
        var destinyApi = new DestinyApi();

        // TODO fix callback hell
        // TODO refactor
        function callback( error, res, body ) {
            if( !error ) {
                var weapons = JSON.parse( body ).Response.data.weapons;
                var responseObj = [];
                var weaponCount = 0;
                weapons.forEach( function( weapon ) {
                    destinyApi.getWeaponInformation( weapon.referenceId, function( error, res, body ) {
                        if( !error ) {
                            var weaponInfo = JSON.parse( body ).Response.data.inventoryItem;

                            responseObj.push({
                                refId: weapon.referenceId,
                                totalKills: weapon.values.uniqueWeaponKills.basic.displayValue,
                                precisionKills: weapon.values.uniqueWeaponPrecisionKills.basic.displayValue,
                                precisionKillPercentage: weapon.values.uniqueWeaponKillsPrecisionKills.basic.displayValue,
                                name: weaponInfo.itemName,
                                image: "https://www.bungie.net" + weaponInfo.icon,
                                desc: weaponInfo.itemDescription,
                                type: weaponInfo.itemTypeName
                            });

                            weaponCount++;
                            if ( weaponCount >= weapons.length ) {
                                response.send( responseObj );
                            }
                        } else {
                            console.log( "Error getting weapon information: ", error );
                        }
                    });
                });
            } else {
                console.log( "Error in weapons call: ", error );
            }
        }
        destinyApi.getExoticWeapons( system, username, callback );
    } else {
        response.send( { "Error": "Validation Error" } );
    }
});

function validateData( username, system ) {
    var isValid = false;
    var regex = /^[a-zA-Z_\- 0-9]+$/;
    
    if ( username.match( regex ) && ( system == '1' || system == '2' ) ) {
        isValid = true;
    }
    
    return isValid;
}

module.exports = router;
