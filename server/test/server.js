var expect = require( "chai" ).expect;
var request = require( "request" );
const baseUrl = "http://localhost:8000/api/v1/";

describe( "REST Endpoints", function() {
    
    describe( "Get Exotic Weapons", function() {
        // Xbox is 1 PS4 is 2
        var url = baseUrl + "weapons?username=test&system=1";
        it( "returns 200", function( done ) {
            request( url, function( error, response, body ) {
               expect( response.statusCode ).to.equal( 200 );
               done();
            });
        });
        
        it( "retrieve exotic weapons for the characters", function( done ) {
            request( url, function( error, response, body ) {
               expect( body ).to.equal( "weapons" );
               done();
            });
        });
    });
    
});
