var fileSystem = require( 'fs' );
var path = __dirname + "/config.json";
var parsedJson = JSON.parse( fileSystem.readFileSync( path, 'UTF-8' ) );

exports.configFile = parsedJson;
