const express = require('express');
const path = require('path');
const http = require('http');
const bodyParser = require('body-parser');

const app = express();

app.use( express.static( path.join( __dirname, 'dist/destiny-app') ) );


const api = require('./server/routes/api');

app.use('/api', api);

app.get('*', function( request, response) {
    response.sendFile( path.join(__dirname, 'dist/destiny-app/index.html' ) );
});

const port = process.env.PORT || '8080';

app.set( 'port', port );

const server = http.createServer( app );

server.listen( port, () => console.log( `Server running on port: ${port}` ) );


